PROJECT_PWD := `pwd`

CASSANDRA_VERSION := 2.1.13
#CASSANDRA_VERSION := 3.1

CASSANDRA_IMG := cassandra:$(CASSANDRA_VERSION)

CONTAINER_CASSANDRA_NAME := cassandra-v$(CASSANDRA_VERSION)
#CONTAINER_CASSANDRA_VOLUME := $(PROJECT_PWD)/ucs-cassandra-db/:/var/lib/cassandra


TEST_NAME := ttl
TESTS_PATH := $(PROJECT_PWD)/tests/$(TEST_NAME)

TESTS_MODEL_PATH := $(TESTS_PATH)/
TESTS_MODEL_CQL := model.cql

TESTS_FIXTURE_PATH := $(TESTS_PATH)/
TESTS_FIXTURE_CQL := fixtures.cql

TESTS_DROP_PATH := $(TESTS_PATH)/
TESTS_DROP_CQL := drop-model.cql

TEST_PATH := $(TESTS_PATH)/
TEST_CQL := test.cql

##########
# DOCKER #
##########

DOCKER := docker

CONTAINER_CASSANDRA_IP := 172.17.0.2

all: docker-cassandra-create

# Tests
#######

docker-cassandra-tests-set-models:
	$(DOCKER) cp $(TESTS_MODEL_PATH)$(TESTS_MODEL_CQL) $(CONTAINER_CASSANDRA_NAME):/$(TESTS_MODEL_CQL)
	$(DOCKER) exec $(CONTAINER_CASSANDRA_NAME) /bin/sh -c 'exec cqlsh -f $(TESTS_MODEL_CQL) $(CONTAINER_CASSANDRA_IP)'

docker-cassandra-tests-set-fixture:
	$(DOCKER) cp $(TESTS_FIXTURE_PATH)$(TESTS_FIXTURE_CQL) $(CONTAINER_CASSANDRA_NAME):/$(TESTS_FIXTURE_CQL)
	$(DOCKER) exec $(CONTAINER_CASSANDRA_NAME) /bin/sh -c 'exec cqlsh -f $(TESTS_FIXTURE_CQL) $(CONTAINER_CASSANDRA_IP)'

docker-cassandra-tests-drop-models:
	$(DOCKER) cp $(TESTS_DROP_PATH)$(TESTS_DROP_CQL) $(CONTAINER_CASSANDRA_NAME):/$(TESTS_DROP_CQL)
	$(DOCKER) exec $(CONTAINER_CASSANDRA_NAME) /bin/sh -c 'exec cqlsh -f $(TESTS_DROP_CQL) $(CONTAINER_CASSANDRA_IP)'

docker-cassandra-tests-run-test:
	$(DOCKER) cp $(TEST_PATH)$(TEST_CQL) $(CONTAINER_CASSANDRA_NAME):/$(TEST_CQL)
	$(DOCKER) exec $(CONTAINER_CASSANDRA_NAME) /bin/sh -c 'exec cqlsh -f $(TEST_CQL) $(CONTAINER_CASSANDRA_IP)'

docker-cassandra-tests-build: docker-cassandra-tests-drop-models docker-cassandra-tests-set-models docker-cassandra-tests-set-fixture

docker-cassandra-tests: docker-cassandra-tests-drop-models docker-cassandra-tests-set-models docker-cassandra-tests-set-fixture docker-cassandra-tests-run-test docker-cassandra-tests-drop-models

docker-cassandra-tests-custom:
	$(DOCKER) cp $(DOCKER_TESTS_CQL) $(CONTAINER_CASSANDRA_NAME):/$(DOCKER_TESTS_CQL)
	$(DOCKER) exec $(CONTAINER_CASSANDRA_NAME) /bin/sh -c 'exec cqlsh -f $(DOCKER_TESTS_CQL) $(CONTAINER_CASSANDRA_IP)'


# CONTAINER
###########

cassandra-ip:
	$(DOCKER) inspect --format='{{ .NetworkSettings.IPAddress }}' $(CONTAINER_CASSANDRA_NAME)

docker-cassandra-create:
	$(DOCKER) run --name $(CONTAINER_CASSANDRA_NAME) -m 2g -d $(CASSANDRA_IMG)

docker-cassandra-start:
	$(DOCKER) start $(CONTAINER_CASSANDRA_NAME)

docker-cassandra-connect:
	$(DOCKER) run -it --link $(CONTAINER_CASSANDRA_NAME) --rm $(CASSANDRA_IMG) sh -c 'exec cqlsh $(CONTAINER_CASSANDRA_IP)'

docker-cassandra-cp-file-to:
	$(DOCKER) cp $(fileToCopyPath)$(fileToCopyName) $(CONTAINER_CASSANDRA_NAME):/$(fileToCopyName)

docker-cassandra-bash:
	$(DOCKER) exec -it $(CONTAINER_CASSANDRA_NAME) bash

docker-cassandra-stop:
	$(DOCKER) stop `docker ps | grep $(CONTAINER_CASSANDRA_NAME) | cut -d ' ' -f1`

docker-cassandra-stop-rm:
	$(DOCKER) stop `docker ps | grep $(CONTAINER_CASSANDRA_NAME) | cut -d ' ' -f1` | xargs $(DOCKER) rm

###########
# /DOCKER #
###########
